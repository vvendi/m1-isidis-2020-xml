async function display() {
    const xml = await get('../xml/countries.xml');
    const xsl = await get('../xsl/country_infos.xsl');
	const country = new URL(location).searchParams.get('country');

    // TODO: xsl transformations
    const proc = new XSLTProcessor();
    proc.importStylesheet(xsl);
	proc.setParameter(null, 'country', country);
    const result = proc.transformToFragment(xml, document);
    
    // TODO: append in #frag_country_list	
	document.querySelector('#frag_country_infos').appendChild(result);
}

display();
